using System.IO;
using System.Linq;
using System.Threading;
using Supernova.Foundation.Core;
using Supernova.Logging;
using Supernova.Logging.Handlers;
using Xunit;

namespace LoggingTests
{
    public class LoggerTests
    {
        [Fact]
        public void Logger_SimpleIntegrationTest()
        {
            Thread.CurrentThread.Name = "main";

            var logger = new Logger();

            const string path1 = "./log1.txt";
            const string path2 = "./log2.txt";

            File.Delete(path1);
            File.Delete(path2);

            logger.Handlers.Add(new TextFileLogHandler(path1));

            logger.Log("Foo");
            logger.Log("Bar");
            logger.Log("Baz");

            logger.Start();

            logger.Log("Hello World!");

            void Fun()
            {
                for (int i = 0; i < 100; i++)
                {
                    logger.Log($"Hello #{i}");
                    Thread.Sleep(10*Randomizer.Next(0,5));
                }
            }

            Thread[] threads = new[]
            {
                new Thread(Fun) { Name = "thread 1" },
                new Thread(Fun) { Name = "thread 2" },
                new Thread(Fun) { Name = "thread 3" },
                new Thread(Fun) { Name = "thread 4" }
            };

            foreach (Thread thread in threads)
            {
                thread.Start();
            }

            Thread.Sleep(500);
            logger.Handlers.Add(new TextFileLogHandler(path2));

            foreach (Thread thread in threads)
            {
                thread.Join();
            }

            string[] lines1 = File.ReadAllLines(path1);
            string[] lines2 = File.ReadAllLines(path2);

            Assert.EndsWith(" | main | Info | Foo", lines1[0]);
            Assert.EndsWith(" | Info | Hello #99", lines1.Last());

            Assert.DoesNotContain("Foo", lines2[0]);
            Assert.EndsWith(" | Info | Hello #99", lines2.Last());

            Assert.Equal(404, lines1.Length);
            Assert.True(lines2.Length < 400);

            logger.Stop();
        }
    }
}
