using System;
using System.Threading;

namespace Supernova.Logging
{
    /// <summary>
    /// A log message.
    /// </summary>
    public class LogMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="logLevel">The log level.</param>
        /// <param name="exception">An optional exception related to the message text.</param>
        public LogMessage(string text, LogLevel logLevel, Exception exception = null)
        {
            Text = text;
            LogLevel = logLevel;
            Time = DateTime.UtcNow;
            ThreadName = Thread.CurrentThread.Name ?? "<unnamed thread>";
            Exception = exception;
        }

        /// <summary>
        /// Gets the text.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Gets the log level.
        /// </summary>
        public LogLevel LogLevel { get; }

        /// <summary>
        ///  Gets the time.
        /// </summary>
        public DateTime Time { get; }

        /// <summary>
        /// Gets the name of the thread from which the message created.
        /// </summary>
        public string ThreadName { get; }

        /// <summary>
        /// Gets the optional exception.
        /// </summary>
        public Exception Exception { get; }
    }
}
