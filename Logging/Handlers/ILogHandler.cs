using System;

namespace Supernova.Logging.Handlers
{
    /// <summary>
    /// An object that can handle a <see cref="LogMessage"/>.
    /// </summary>
    public interface ILogHandler : IDisposable
    {
        /// <summary>
        /// Handles a log message.
        /// </summary>
        /// <param name="message">The log message.</param>
        void Handle(LogMessage message);
    }
}
