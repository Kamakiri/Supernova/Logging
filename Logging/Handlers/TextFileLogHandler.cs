using System.IO;
using Supernova.Foundation.Core;
using Supernova.Logging.Formatters;

namespace Supernova.Logging.Handlers
{
    public class TextFileLogHandler : ILogHandler
    {
        private readonly string _path;
        private readonly ILogMessageFormatter _formatter;

        public TextFileLogHandler(string path, ILogMessageFormatter formatter)
        {
            Guard.NotNullOrWhitespace(path, nameof(path));
            Guard.NotNull(formatter, nameof(formatter));

            _path = path;
            _formatter = formatter;
        }

        public TextFileLogHandler(string path) : this(path, new DefaultLogMessageFormatter())
        {
        }

        public void Dispose() { }

        public void Handle(LogMessage message) => File.AppendAllText(_path, _formatter.Format(message));
    }
}
