using System.Globalization;
using System.Text;

namespace Supernova.Logging.Formatters
{
    public class DefaultLogMessageFormatter : ILogMessageFormatter
    {
        public string Format(LogMessage message)
        {
            var builder = new StringBuilder();

            string time = message.Time.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
            builder.AppendLine($"{time} | {message.ThreadName} | {message.LogLevel} | {message.Text}");

            if (message.Exception != null)
            {
                builder.AppendLine(message.Exception.ToString());
            }

            return builder.ToString();
        }
    }
}
