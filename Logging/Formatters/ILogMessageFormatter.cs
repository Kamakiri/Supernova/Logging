namespace Supernova.Logging.Formatters
{
    public interface ILogMessageFormatter
    {
        string Format(LogMessage message);
    }
}
