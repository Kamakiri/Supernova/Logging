namespace Supernova.Logging
{
    /// <summary>
    /// Represents the log level of log message.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// Describes an unrecoverable application or system crash, or a catastrophic failure.
        /// </summary>
        Fatal,

        /// <summary>
        /// Describes a failure in the current flow of execution but not an application-wide failure.
        /// </summary>
        Error,

        /// <summary>
        /// Describes an abnormal or unexpected event in the application flow that does not cause the application execution to stop.
        /// </summary>
        Warn,

        /// <summary>
        /// Describes a normal flow or behavior.
        /// </summary>
        Info,

        /// <summary>
        /// Describes debugging information.
        /// </summary>
        Debug,

        /// <summary>
        /// Describes detailed data about the application flow for advanced debugging.
        /// </summary>
        Trace
    }
}
