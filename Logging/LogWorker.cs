using System;
using System.Threading;
using Supernova.Foundation.Core;
using Supernova.Logging.Handlers;

namespace Supernova.Logging
{
    /// <summary>
    /// A log worker that processes log messages from the logger queue.
    /// </summary>
    internal class LogWorker
    {
        private readonly Logger _logger;

        private readonly object _startLock = new object();

        private Thread _thread;

        public LogWorker(Logger logger)
        {
            Guard.NotNull(logger, nameof(logger));
            _logger = logger;
        }

        internal bool Running { get; set; } = false;

        public void Start()
        {
            lock (_startLock)
            {
                if(_thread == null)
                {
                    Running = true;
                    _thread = new Thread(HandleMessages)
                    {
                        Name = $"{nameof(LogWorker)} thread {Guid.NewGuid()}",
                        IsBackground = true
                    };
                    _thread.Start();
                }
            }
        }

        private void HandleMessages()
        {
            while (Running)
            {
                LogMessage message = _logger.Messages.Take();

                foreach (ILogHandler handler in _logger.Handlers)
                {
                    handler.Handle(message);
                }

                _logger.Adapt();
            }
        }
    }
}
