using System;
using System.Collections.Concurrent;
using Supernova.Logging.Handlers;

namespace Supernova.Logging
{
    /// <summary>
    /// The entry point to the log system. Non blocking.
    /// </summary>
    public class Logger
    {
        private readonly object _startLock = new object();
        private readonly object _workersLock = new object();
        private readonly AdaptativeWorkerStrategy _workerStrategy;
        private bool _running = false;

        public Logger(AdaptativeWorkerStrategy workerStrategy = null)
        {
            _workerStrategy = workerStrategy;
        }

        /// <summary>
        /// Gets the handlers that will handle each log message added to the queue.
        /// </summary>
        public BlockingCollection<ILogHandler> Handlers { get; } = new BlockingCollection<ILogHandler>();

        internal BlockingCollection<LogMessage> Messages { get; } = new BlockingCollection<LogMessage>();

        internal BlockingCollection<LogWorker> Workers { get; } = new BlockingCollection<LogWorker>();

        /// <summary>
        /// Adds a <see cref="LogWorker"/> to the active workers.
        /// </summary>
        /// <exception cref="InvalidOperationException">Throws if the <see cref="Logger"/> is not running.</exception>
        public void AddWorker()
        {
            lock (_workersLock)
            {
                AssertIsRunning();

                var worker = new LogWorker(this);
                Workers.Add(worker);
                worker.Start();
            }
        }

        /// <summary>
        /// Add a new log message to the log queue.
        /// </summary>
        /// <param name="text">The message text.</param>
        /// <param name="logLevel">The message log level.</param>
        public void Log(string text, LogLevel logLevel = LogLevel.Info) => Messages.Add(new LogMessage(text, logLevel));

        /// <summary>
        /// Removes a <see cref="LogWorker"/> from the active workers.
        /// If the workers count reacher 0, the <see cref="Logger"/> stops.
        /// </summary>
        /// <exception cref="InvalidOperationException">Throws if the <see cref="Logger"/> is not running.</exception>
        public void RemoveWorker()
        {
            lock (_workersLock)
            {
                AssertIsRunning();

                LogWorker worker = Workers.Take();
                worker.Running = false;
                if (Workers.Count == 0) _running = false;
            }
        }

        /// <summary>
        /// Starts the <see cref="Logger"/> with one <see cref="LogWorker"/>.
        /// </summary>
        public void Start()
        {
            lock (_startLock)
            {
                if (!_running && Workers.Count == 0)
                {
                    var worker = new LogWorker(this);
                    Workers.Add(worker);
                    worker.Start();
                    _running = true;
                }
            }
        }

        /// <summary>
        /// Stops the <see cref="Logger"/> and all its <see cref="LogWorker"/>.
        /// </summary>
        /// <exception cref="InvalidOperationException">Throws if the <see cref="Logger"/> is not running.</exception>
        public void Stop()
        {
            lock (_workersLock)
            {
                AssertIsRunning();

                while (Workers.Count > 0)
                {
                    LogWorker worker = Workers.Take();
                    worker.Running = false;
                }
                _running = false;
            }
        }

        /// <summary>
        /// Adapts the number of active workers according to the adaptive worker strategy, if any.
        /// </summary>
        internal void Adapt()
        {
            if (_workerStrategy == null) return;

            int messagesCount = Messages.Count;
            int workersCount = Workers.Count;
            double messagesPerWorker = messagesCount / workersCount;

            while (true)
            {
                if (messagesPerWorker > _workerStrategy.MaxMessagesPerWorker
                    && workersCount < _workerStrategy.MaxWorkersCount) AddWorker();
                else if (messagesPerWorker < _workerStrategy.MinMessagesPerWorker
                    && workersCount > _workerStrategy.MinWorkersCount) RemoveWorker();
                else break;
            }
        }

        private void AssertIsRunning()
        {
            if (!_running) throw new InvalidOperationException("Logger is not running");
        }
    }
}
