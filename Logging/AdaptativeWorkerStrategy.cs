using Supernova.Foundation.Core;

namespace Supernova.Logging
{
    public class AdaptativeWorkerStrategy
    {
        public AdaptativeWorkerStrategy(
            int minWorkersCount,
            int maxWorkersCount,
            int minMessagesPerWorker,
            int maxMessagesPerWorker)
        {
            Guard.GreaterThanOrEqualTo(minWorkersCount, maxWorkersCount, nameof(maxWorkersCount));
            Guard.GreaterThanOrEqualTo(minMessagesPerWorker, maxMessagesPerWorker, nameof(maxMessagesPerWorker));

            MinWorkersCount = minWorkersCount;
            MaxWorkersCount = maxWorkersCount;
            MinMessagesPerWorker = minMessagesPerWorker;
            MaxMessagesPerWorker = maxMessagesPerWorker;
        }

        public int MaxMessagesPerWorker { get; }

        public int MaxWorkersCount { get; }

        public int MinMessagesPerWorker { get; }

        public int MinWorkersCount { get; }
    }
}
